import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import numpy as np

class SoftmaxClassifierModel(nn.Module) :
  def __init__(self) :
    super().__init__()
    self.linear = nn.Linear(6,6) # 4->3

  def forward(self, x) :
    return self.linear(x)

model = SoftmaxClassifierModel()

csv_train = np.loadtxt('STD_Train.csv', delimiter = ',', dtype = np.float32)
csv_test = np.loadtxt('STD_Test.csv', delimiter = ',', dtype = np.float32)

train_np_x = csv_train[:,:-1] # 마지막 직전까지의 cloumn
train_np_y = csv_train[:,[-1]] # 마지막 column

test_np_x = csv_test[:,:-1] # 마지막 직전까지의 column
test_np_y = csv_test[:,[-1]] # 마지막 column

x_train = torch.from_numpy(train_np_x)
y_train = torch.from_numpy(train_np_y).squeeze(1).long()
x_test = torch.from_numpy(test_np_x)
y_test = torch.from_numpy(test_np_y).squeeze(1).long()

optimizer = optim.SGD(model.parameters(), lr = 0.02)

epochs = 80000
for epoch in range(epochs + 1):
  hypothesis = model(x_train)
  cost = F.cross_entropy(hypothesis, y_train)

  optimizer.zero_grad()
  cost.backward()
  optimizer.step()

  if epoch % 100 == 0 :
    print('Epoch {:4d}/{} Cost: {:.6f}'.format(epoch, epochs, cost.item()))

prediction = torch.argmax(model(x_test),1) == y_test
print(prediction)
print(prediction.float().mean())
